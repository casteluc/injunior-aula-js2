var l1 = [1, 1, 2, 3]
var l2 = [2, 3, 1, 1]

const arrayIsEqual = (list1, list2) => {
    list1.sort()
    list2.sort()

    var isEqual = true
    for (i = 0; i < list1.length; i++) {
        if (list1[i] != list2[i]) {
            isEqual = false
        }
    }

    return isEqual
}

console.log(arrayIsEqual(l1, l2))
