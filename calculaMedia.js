/* 
    EXERCÍCIO DE JS - TREINAMENTO INJUNIOR
    Programa que calcula quem foi o aluno com média mais alta em cada turma
*/

const printAverage = studentsArray => {

    // Declara as variáveis que vão ser usadas posteriormente percorrendo a lista de alunos
    let biggerAverageA = 0
    let indexA = 0

    let biggerAverageB = 0
    let indexB = 0

    let biggerAverageC = 0
    let indexC = 0

    // Confere pra cada aluno se a nota média dele é maior do que a média de alguns de seus colegas de turma,
    // se for verdadeiro, ele guarda o valor dessa média para conferir com os outros alunos depois e guarda o índice
    // desse aluno para ele poder ser recuperado posteriormente
    studentsArray.forEach( (student, i) => {
        let studentAverage = (student["nota1"] + student["nota2"]) / 2

        if (student["turma"] == "A" && studentAverage > biggerAverageA) {
            biggerAverageA = studentAverage
            indexA = i
        } else if (student["turma"] == "B" && studentAverage > biggerAverageB) {
            biggerAverageB = studentAverage
            indexB = i
        } else if (student["turma"] == "C" && studentAverage > biggerAverageC) {
            biggerAverageC = studentAverage
            indexC = i
        }
    })

    // Cria uma lista com o index dos alunos com maior média de cada turma e imprime uma frase na tela
    // para cada um desses alunos, informando todo o conteúdo do objeto
    indexes = [indexA, indexB, indexC]
    indexes.forEach(i => {
        average = (studentsArray[i]["nota1"] + studentsArray[i]["nota2"]) / 2
        text = "O aluno " + studentsArray[i]["nome"] + " teve a média mais alta da turma " + studentsArray[i]["turma"] + ", com " + average
        console.log(text)
    })
}

// Declaração do array de alunos
testArray = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    }
]

// Chamando a função que imprime a media dos alunos
printAverage(testArray)